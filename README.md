# GitlabTimeSpend

A simple command line tool to book time to gitlab issues.

Project status: Works on my machine

Tested on GitBash (Windows).

## Usage

### Install the package
```
npm i -g gitlabtimespend
```
### Get a gitlab token with api access.

You can find it at a url like this 'https://gitlab.com/-/profile/personal_access_tokens'.

### Configure your token
```
$ spend token <your-token-here>
```
for example
```
$ spend token asduhfdf234kjhdf
```
### Configure your gitlab domain
```
$ spend domain <your-gitlab.domain>
```
for example
```
$ spend domain gitlab.example.com
```
### Book your time
Switch to a directory where you have checked out an feature-branch.
The branch name should look like this "20-this-is-my-feature".

And book your time with

```
$ spend <gitlab-time-unit-string>
```

for example
```
$ spend 2h30m
```

see https://docs.gitlab.com/ee/user/project/time_tracking.html#view-a-time-tracking-report
